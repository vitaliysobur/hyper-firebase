import C from '../constants';

export const addNotification = (notification) => {
  return {
    type: C.ADD_NOTIFICATION,
    data: notification
  }
}

export const addTerm = (term) => {
  return {
    type: C.ADD_TERM,
    data: term
  }
}
