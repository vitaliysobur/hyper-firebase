import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const App = (props) => {
  return (
    <MuiThemeProvider>
      <div>
        <header className="app-header">
          <div className="container">
            <div className="logo"></div>
            <h1>Hyper Firebase!</h1>
          </div>
        </header>
        <section className="container app-section">
          { props.children }
        </section>
      </div>
    </MuiThemeProvider>
  );
};

export default App;
