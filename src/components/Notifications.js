import React from 'react';

const Notifications = ({
  notifications
}) => {
  return (
    <div className="notifications">
      <ul>{notifications.map(item => {
        return <li>{item}</li>
      })}</ul>
    </div>
  );
};

export default Notifications;