import React from 'react';

const Term = ({
  terms,
  changeHandler
}) => {
  return (
    <div className="term">
      <input type="text" onKeyUp={changeHandler} defaultChecked={terms} />
    </div>
  );
};

export default Term;