import React from 'react';
import Term from '../components/Term';
import Notifications from '../components/Notifications';

const Terminal = ({
  terms,
  notifications,
  changeHandler
}) => {
  return (
    <div className="terminal">
      <Notifications notifications={notifications} />
      <Term terms={terms} changeHandler={changeHandler} />
    </div>
  );
};

export default Terminal;