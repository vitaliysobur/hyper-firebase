import React, { Component } from 'react';
import { connect } from 'react-redux';
import App from '../components/App';
import firebase from 'firebase';
import { addNotification } from '../actions/actions';
require('dotenv').config({path: '.env/local'});

const config = {
  apiKey: "AIzaSyDsV2CyNmko4AB2Tmmr06P5eJYxO3l1QAM",
  authDomain: "hyper-firebase.firebaseapp.com",
  databaseURL: "https://hyper-firebase.firebaseio.com",
  projectId: "hyper-firebase",
  storageBucket: "hyper-firebase.appspot.com",
  messagingSenderId: "670723844205"
};

firebase.initializeApp(config);
const notificationsRef = firebase.database().ref('notifications');

class AppContainer extends Component {
  componentWillMount() {
    const { dispatch } = this.props;

    notificationsRef.on('child_added', snap => {
      dispatch(addNotification(snap.val()));
    });
  }

  render() {
    return (
      <App {...this.props} />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps
  }
}

export default connect(mapStateToProps)(AppContainer);
