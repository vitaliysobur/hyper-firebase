import React, { Component } from 'react';
import { connect } from 'react-redux';
import Terminal from '../components/Terminal';
import { addTerm, addNotification } from '../actions/actions';
import firebase from 'firebase';

// const config = {
//   apiKey: "AIzaSyDsV2CyNmko4AB2Tmmr06P5eJYxO3l1QAM",
//   authDomain: "hyper-firebase.firebaseapp.com",
//   databaseURL: "https://hyper-firebase.firebaseio.com",
//   projectId: "hyper-firebase",
//   storageBucket: "hyper-firebase.appspot.com",
//   messagingSenderId: "670723844205"
// };

// firebase.initializeApp(config);

const termRef = firebase.database().ref('terms');
const notificationsRef = firebase.database().ref('notifications');

class TerminalContainer extends Component {
  componentWillMount() {
    // const { dispatch } = this.props;

    // notificationsRef.on('child_added', snap => {
    //   dispatch(addNotification(snap.val()));
    // });
  }

  render() {
    return (
      <Terminal {...this.props} changeHandler={this.props.changeHandler} />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    terms: state.terms,
    notifications: state.notifications
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changeHandler(e) {
      var keys = {
        65: 'a',
        66: 'b',
        67: 'c',
        68: 'd',
        69: 'e',
        70: 'f',
        71: 'g',
        72: 'h',
        73: 'i',
        74: 'j',
        75: 'k',
        76: 'l',
        77: 'm',
        78: 'n',
        79: 'o',
        80: 'p',
        81: 'q',
        82: 'r',
        83: 's',
        84: 't',
        85: 'u',
        86: 'v',
        87: 'w',
        88: 'x',
        89: 'y',
        90: 'z',
        189: '-',
        32: ' '
      }

      if (keys[e.keyCode]) {
        termRef.push().set(keys[e.keyCode]);
        return dispatch(addTerm(keys[e.keyCode]));
      }

      if (e.keyCode === 8) {
        termRef.push().set(unescape('%7F'));
        return dispatch(addTerm(unescape('%7F')));
      }

      if (e.keyCode === 13) {
        e.target.value = '';

        termRef.push().set('\r');
        return dispatch(addTerm('\r'));
      }
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TerminalContainer);
