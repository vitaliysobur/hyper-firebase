import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { Router, Route, IndexRoute } from 'react-router';
import { createStore, applyMiddleware } from 'redux';

import history from './history';
import rootReducer from './reducers';

import AppContainer from './containers/AppContainer';
import TerminalContainer from './containers/TerminalContainer';

import injectTapEventPlugin from 'react-tap-event-plugin';
import './css/styles.css';

injectTapEventPlugin();

const store = process.env.NODE_ENV === 'development' ?
  applyMiddleware(thunkMiddleware)(createStore)(
    rootReducer,
    window.devToolsExtension && window.devToolsExtension()
  ) :
  applyMiddleware(thunkMiddleware)(createStore)(rootReducer);

render(
  <Provider store={ store }>
    <Router history={ history }>
      <Route path="/" component={ AppContainer }>
        <IndexRoute component={ TerminalContainer } />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
