import { combineReducers } from 'redux';
import notificationsReducer from './notificationsReducer';
import termReducer from './termReducer';

export default combineReducers({
  notifications: notificationsReducer,
  terms: termReducer
});
