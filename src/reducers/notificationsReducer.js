import C from '../constants';

const initialState = [];

const notificationsReducer = (currentState = initialState, action) => {
  switch(action.type) {
    case C.ADD_NOTIFICATION:
      return [...currentState, [action.data]];

    default:
      return currentState;
  }
}

export default notificationsReducer;
