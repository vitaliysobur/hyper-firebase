import C from '../constants';

const initialState = '';

const termReducer = (currentState = initialState, action) => {
  switch(action.type) {
    case C.ADD_TERM:
      return (currentState += action.data);

    default:
      return initialState;
  }
}

export default termReducer;
